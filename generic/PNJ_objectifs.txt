cherche à nuire à %PNJ
amoureux fou de %PNJ
recherche %PNJ
travaille pour %FACTION
voudrait rejoindre %FACTION
voudrait créer %FACTION
lutte contre l'influence de %FACTION
veut tuer %PNJ
cherche à soudoyer %PNJ
cherche à protéger %PNJ
se fait passer pour %PNJ
veut connaître les secrets de %PNJ
conspire avec %PNJ
se venger de %PNJ / se venger de %FACTION

cherche à se faire de l'argent
veut fabriquer un chef d'œuvre
retrouver %MCGUFFIN
veut sauver %PNJ d'une maladie rare
veut prouver sa valeur à %PNJ
cherche à croitre en puissance
accumuler du pouvoir
protéger la loi
gagner des terres
profite de la vie
nettoyer l'honneur de %FACTION / nettoyer le nom de sa famille
veut découvrir la vérité sur %FACTION / veut découvrir la vérité sur %PNJ
veut manipuler %PNJ pour un travail sordide pour le compte de %FACTION
veut devenir un maître de son art
cherche à devenir un héros
voudrait détruire %MCGUFFIN
veut rétablir la suprématie de %FACTION
cherche à mourir glorieusement
cherche à réparer ses erreurs
cherche à échapper à %PNJ / cherche à échapper à %FACTION

veut quitter %LIEU
veut cambrioler %LIEU
veut visiter %LIEU
veut acheter %LIEU
veut s'emparer de %LIEU
cherche à rejoindre %LIEU
cherche à construire %LIEU
veut protéger %LIEU
veut vendre %LIEU
veut transformer %LIEU
