# Ceci repart de intrigue_principal.txt, les points 2,3,4,5,7,8,10,11,12,13,14,15,16,17,18,19,20,23,26
# 

Au cours d'un voyage, les PJs decouvrent %LIEU où %FACTION %OBJECTIF, ils doivent donc retourner voir %ALLIE au plus vite pour l'en avertir.
Par le plus grand hasard, les PJs decouvrent %LIEU où %FACTION %OBJECTIF, bloqués par %ENNEMI, ils doivent intervenir rapidement en solitaire pour éviter le pire.
%LIEU est récemment tombé sous la coupe de %FACTION et cela ne sied pas du tout à %ALLIE, un grand ami des PJs qui leur demande donc d'intervenir.
Afin de %OBJECTIF, %ALLIE a besoin de découvrir %LIEU et demande aux PJs d'enquêter sur ce dernier et d'en obtenir le contrôle.
Afin de %OBJECTIF, %ALLIE a besoin de découvrir %MCGUFFIN et demande aux PJs d'enquêter sur ce dernier et de l'obtenir par tous les moyens.
%FACTION est en train de prendre %LIEU ce qui ne serait pas si grave si %ALLIE ne s'y trouvait pas. Il faut donc évacuer ce dernier de toute urgence.
%ENNEMI a obtenu des informations sur les personnages et veut les faire chanter. Il les force donc à %OBJECTIF ce que les personnages ne veulent évidemment pas.
%ALLIE a reçu un message le forçant à %OBJECTIF sous peine de divulguer des informations compromettantes et demande au PJ d'enquêter afin de découvrir le comanditaire: %ENNEMI.
L'un des PJs %OBJECTIF, or il s'avère que %ENNEMI aussi. Peut-on trouver un terrain d'entente ?
%ENNEMI s'est échappé de la garde de %ALLIE. Il s'est réfugié dans %LIEU où seuls les personnages peuvent aller le chercher.
%ALLIE veut absoluement récupérer %MCGUFFIN, mais bien sûr, %ENNEMI n'est pas en reste car %FACTION le lui aussi a demandé.
Les PJs ont récemment récupéré %MCGUFFIN que convoite %FACTION. Ce dernier va donc envoyer %ENNEMI pour le reprendre.
Afin d'échapper à %ENNEMI, les PJs doivent rejoindre %LIEU au plus vite.
Un grand concours est organisé et les PJs veulent y participer. Si %ENNEMI est leur principal concurrent, %FACTION cherche à tricher pour son propre compte.
%ENNEMI cherche à se rendre à %LIEU et les PJs doivent l'en empêcher.
Les PJs se retrouve dans %LIEU à la demande de %ALLIE afin d'empêcher %ENNEMI d'en prendre le contrôle pour le compte de %FACTION.
En voyage, les PJs sont victimes avec le reste de leur convoi d'un détournement orchestré par %ENNEMI.
%MCGUFFIN a récemment été découvert dans %LIEU et %ENNEMI cherche à la récupérer. Les PJs pourront-ils l'en empécher.
%ALLIE à demander au PJs d'ouvrir des pourparlers avec %FACTION afin de les empécher de réaliser leur plan. Il se retrouve pour cela dans une place neutre: %LIEU.
%ALLIE veut absoluement récupérer %MCGUFFIN, mais ce dernier se trouve dans %LIEU, gardé par %ENNEMI ce qui ne rends pas la chose aisée.
%ENNEMI à tué %ALLIE pour récupérer %MCGUFFIN. Les PJs doivent donc mener l'enquête pour le démasquer et le livrer à la garde.
%ALLIE est accusé à tort d'un crime commis par %ENNEMI. Les PJs sont donc mandatés pour l'innocenté.
%ALLIE veut absoluement que %MCGUFFIN arrive à %LIEU sans encombre et demande aux PNJs de l'escorter. Malheureusement %FACTION à eu vent de ce transport et compte bien en profiter pour faire main basse sur ce colis.
Pour masquer leurs agissements, %FACTION ont mis en place une couverture quelque peu étonnante. %ALLIE demande donc aux PJs d'enquêter.
Les PJs sont partis explorer %LIEU mystérieux pour le compte de %ALLIE. %FACTION qui y a des intérêts pourrait bien leur jouer un mauvais tour.
%ENNEMI mène une série de crime dans la région. %ALLIE demande donc au PJs d'intervenir au plus vite.
%ALLIE demande aux PJs de prendre la tête de %FACTION pour quelque temps. Cela ne plait pas du tout à %ENNEMI qui fera tout pour rendre la vie insuportable à nos aventuriers.
%ALLIE previent les PJs que %ENNEMI %OBJECTIF. Il faut donc que ces derniers l'en empèche au plus vite.
Les PJs se sont (sans le savoir) mis à dos %FACTION lors de leur dernière aventure. On a donc choisi %ENNEMI pour tenter de les éliminer.
Après s'être emparé de %MCGUFFIN au nez et à la barbe des PJs, %ENNEMI s'est enfuit au plus vite vers %LIEU. Les PJs partent donc en chasse pour aujourd'hui.
Trompé par %FACTION, %PNJ s'est retourné contre les PJs. Aux PJs de prouver leur innocence.
Pris au piège dans %LIEU, les PJs se retrouvent coincés avec %ENNEMI caché non loin. Ce dernier en profite alors pour voler ou attaquer nos héros.
%FACTION demande aux PJs de comprendre pourquoi %LIEU a mauvaise réputation. Ils y découvrent %ENNEMI qui n'aime pas les fouineurs.
Les PJs doivent simplement traverser %LIEU, mais cela s'avère plus difficile que prévu.
%LIEU où se reposent les PJs est soudain attaqué par %ENNEMI. Il faut appeler les renforts et tenir la place en attendant !
Les PJs tombent sur %LIEU dévasté !
