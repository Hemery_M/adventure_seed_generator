neuf / ancien
glacé / froid / chaud / brûlant
immense / grand / petit / minuscule
reculé / innaccessible / secret
noir / blanc / rouge / vert
en piteux état / dans une propreté impecable
sombre / lumineux
humide / sec
