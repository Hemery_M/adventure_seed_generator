"""
Test module for the different function and routine
"""
import unittest
import define_class as dc
dc.SOURCE = 'fantasy/'

class TestFunction(unittest.TestCase):
    """Mother class for all tests"""
    def test_parser(self):
        self.assertEqual(dc.parser('test.txt'),
                         [["test11"],["test21","test22"]])

if __name__ == '__main__':
    unittest.main()