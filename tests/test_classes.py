"""
Test module for the different class
"""
import unittest
import define_class as dc
dc.SOURCE = 'fantasy/'

class TestElement(unittest.TestCase):
    """Test of the mother Element class"""
    def setUp(self):
        self.el = dc.Element()
    
    def test_short_desc(self):
        self.assertEqual(self.el.short_description(),'je suis dummy')
    
    def test_long_desc(self):
        self.assertEqual(self.el.long_description(),'cet element est dummy')
    
    def test_str(self):
        self.assertEqual(self.el.__str__(),'je suis dummy')
        self.assertEqual(self.el.__str__(),'dummy')

if __name__ == '__main__':
    unittest.main()