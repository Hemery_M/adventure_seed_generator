#coding: utf-8
import define_class as dc
import optparse as op
import random as rd
import re

# Direct Generator
def generate_objectif(nombre=1): #Il faut encore utiliser nombre
    return dc.choix('faction_objectifs.txt')
dc.key_word['%OBJECTIF'] = generate_objectif

def generate_intrigue():
    return dc.choix('intrigues.txt')
dc.key_word['%INTRIGUE'] = generate_intrigue

def generate_all():
    history = "+++ L'histoire: +++\n"
    int1 = generate_intrigue()
    history += dc.key_word_filler(int1)

    # Listing of the different elements
    def add_element_desc(type,name):
        """Return the long description of every element of a class type"""
        desc = ""
        if type._list:
            desc +=  "\n|Liste des {0}|\n".format(name)
            for pnj in type._list:
                desc += pnj.long_description()+'\n'
        return desc
        
    history += "\n\n+++ Les éléments: +++"
    history += add_element_desc(dc.PNJ,'PNJs')
    history += add_element_desc(dc.Faction,'factions')
    history += add_element_desc(dc.Lieu,'lieux')
    history += add_element_desc(dc.McGuffin,'McGuffins')
    
    return history

### MAIN ###
if __name__ == "__main__":
    # Options definition
    parser = op.OptionParser()
    parser.add_option("-s", "--source", action="store", help="Modify the directory for the sources (default = fantasy)")
    parser.add_option("-o","--output",action="store", help="Name a file to save the output (default = go to the prompt)")
    (options, args) = parser.parse_args()

    if options.source: dc.SOURCE_SUP = 'generateur/'+options.source+'/'
    if options.output:
        with open(options.output+'.txt','wb') as my_output:
            my_output.write(generate_all())
    else:
        print(generate_all())
