#coding: utf-8
"""
Define all the needed class to the description of the elements of the adventure: PNJ – 
Coder : M. Hemery
Created/Last Edition : 2016-06-25
"""
import os
import random as rd
import re

# Variable generale
SOURCE = 'generateur/generic/'
SOURCE_SUP = ''
world_size = 10 #determine the trade-of between the generation of new items and the re-using of old ones
key_word = {}

# General Function
def parser(title):
    data = []
    with open(SOURCE+title,'r') as doc:
        for line in doc.readlines():
            if line[:-1] and line[0] != '#':
                data.append([item.strip() for item in line.split('/')])
    if os.path.isfile(SOURCE_SUP+title):
        with open(SOURCE_SUP+title,'r') as doc:
            for line in doc.readlines():
                if line[:-1] and line[0] != '#':
                    data.append([item.strip() for item in line.split('/')])
    return data

def key_word_filler(line):
    KW = re.search(r"\%\w*",line)
    while KW:
        kw = KW.group()
        new = key_word[kw]()
        line = line.replace(kw,str(new),1)
        KW = re.search(r"\%\w*",line)
    return line

def choix(*titles,sample=1):
    pool = []
    for title in titles:
        pool += parser(title)
    
    choix1 = rd.sample(pool,sample)
    line = ' et '.join([rd.choice(ch1).strip() for ch1 in choix1])
    return key_word_filler(line)

def hill_world_size(n_act,n_max = world_size):
    """ Determine if a new element should be creted or an old one used """
    return float(n_act)/(n_act+n_max) < rd.random()

def new_item(item,*args):
    """ Create a new item if needed or return an old one """
    if hill_world_size(len(item._list)):
        return item(*args)
    else:
        return rd.choice(item._list)

class Element(object):
    """Main class for the element of the adventure"""
    def __init__(self):
        self.name = 'dummy'
        self.first_occ = 1
        self.short_desc = 'je suis {0.name}'
        self.long_desc = 'cet element est {0.name}'
    
    def __str__(self):
        if self.first_occ:
            return self.short_description()
        else:
            return self.name
    
    def short_description(self):
        """A short description of the element for its first occurence in the text adventure"""
        self.first_occ = 0
        return self.short_desc.format(self)
        
    def long_description(self):
        """A long description of the element to display at the end of the adventure"""
        return self.long_desc.format(self)

class PNJ(Element):
    _list = []
    
    def __init__(self):
        super().__init__()
        PNJ._list.append(self)
        self.name = "PNJ#{}".format(len(PNJ._list))
        self.type = choix('PNJs.txt')
        self.detail = choix('PNJ_details.txt')
        self.objectif = choix('PNJ_objectifs.txt')
        self.short_desc = "{0.type} {0.detail} ({0.name})"
        self.long_desc = '{0.name} est {0.type} {0.detail} qui {0.objectif}.'
def new_PNJ(): return new_item(PNJ)
key_word['%PNJ'] = new_PNJ

class Ennemi(PNJ):
    def __init__(self):
        super().__init__()
        self.type = choix('PNJs.txt', 'Ennemis.txt')
def new_Ennemi(): return new_item(Ennemi)
key_word['%ENNEMI'] = new_Ennemi

class Allie(PNJ):
    def __init__(self):
        super().__init__()
        self._type = choix('PNJs.txt', 'Allies.txt')
def new_Allie(): return new_item(Allie)
key_word['%ALLIE'] = new_Allie

class McGuffin(Element):
    _list = []

    def __init__(self):
        super().__init__()
        McGuffin._list.append(self)
        self.name = "McGuffin#{}".format(len(McGuffin._list))
        self.type = choix('McGuffins.txt')
        self.short_desc = "{0.name}, {0.type},"
        self.long_desc = "{0.type}"
key_word["%MCGUFFIN"] = McGuffin

class Faction(Element):
    _list = []

    def __init__(self):
        super().__init__()
        Faction._list.append(self)
        self.name = "Faction#{}".format(len(Faction._list))
        self.type = choix('factions.txt')
        self.qualificatif = choix('faction_qualificatifs.txt')
        self.objectif = choix('faction_objectifs.txt')
        self.short_desc = "{0.name}, {0.type} {0.qualificatif},"
        self.long_desc = "{0.name} est {0.type} {0.qualificatif} qui {0.objectif}."
def new_Faction(): return new_item(Faction)
key_word["%FACTION"] = new_Faction

class Lieu(Element):
    _list = []

    def __init__(self):
        super().__init__()
        Lieu._list.append(self)
        self.name = "Lieu#{}".format(len(Lieu._list))
        self.type = choix('lieux.txt')
        self.qualificatif = choix('lieu_qualificatifs.txt')
        self.short_desc = "{0.type} ({0.name}),"
        self.long_desc = "{0.name} est {0.type} {0.qualificatif}"
def new_Lieu(): return new_item(Lieu)
key_word["%LIEU"] = new_Lieu
