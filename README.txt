Ce programme est en cours d'élaboration, ce n'est donc pas une surprise si vous avez des difficultés à l'utiliser.
En très gros placez vous devant le projet et invoquez 'python generateur/', je m'essaierai à faire un GUI quand j'en trouverais le temps ;).

Si toutefois vous avez des suggestions ou souhaitez participer à sa conception, écrivez moi à pal'at'melix.org.